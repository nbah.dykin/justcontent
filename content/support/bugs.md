---
title: "Баги"
description: "Помощь по репорту багов на сервере JustMC."
---

Нашли баг? Опишите его [здесь](https://gitlab.com/justmc/justmc/-/issues):

1. Зарегистрируйтесь или войдите (Если вы этого не сделали)

![Справа сверху](https://media.discordapp.net/attachments/775789624262000641/794560660695810048/unknown.png)

2. Создайте новый ишьюc

![Чуть ниже кнопки регистрации](https://cdn.discordapp.com/attachments/775789624262000641/794561260745261109/unknown.png)

3. Опишите свой баг по предоставленному шаблону

![Шаблон](https://media.discordapp.net/attachments/775789624262000641/794561881695060000/unknown.png)

4. Выберите конфиденциальность бага (Видно для всех или только для разработчиков)

![Под описанием бага](https://media.discordapp.net/attachments/775789624262000641/794562035878854668/unknown.png)

5. Опубликуйте баг

![Слева снизу от описания бага](https://media.discordapp.net/attachments/775789624262000641/794562268040921088/unknown.png)
